#! /usr/bin/env python

#copy write (c) 2018,willow grage, Inc.
#All rights reserved 
#Copyright <2018> <thoufeeque s>
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE

from __future__ import print_function
import rospy
import actionlib
import sys


import action_client_application.msg

def person_client(y):
#Creates the SimpleActionClient, passing the type of the action

    client = actionlib.SimpleActionClient('details', action_client_application.msg.PersonAction)
    
# listening for goals.
    client.wait_for_server()
    

    goal = action_client_application.msg.PersonGoal(name=y)
# Sends the goal to the action server.
    client.send_goal(goal)
# Waits for the server to finish performing the action.
    client.wait_for_result()

# Prints out the result of executing the action
    return client.get_result() 
#start of main function
if __name__ == '__main__':
 
  while (1):
    
    try:
        name=raw_input("Enter the name of a person  ")
        print(name)
# Initializes a rospy node so that the SimpleActionClient can

        rospy.init_node('person_client')
        result = person_client(name)
	print("name:",name)
	if result.sequence==0:
		print ("sorry no details found")
	else:

		
		print("Age:",result.sequence)
    except rospy.ROSInterruptException:
        print("program interrupted before completion", file=sys.stderr)
